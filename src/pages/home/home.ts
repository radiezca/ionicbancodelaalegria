import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	sucursales: AngularFirestoreCollection <any>;
 	sucursales$: any;
	sucursalSeleccionada : any;

	franjas: AngularFirestoreCollection <any>;
	franjas$: any;
	franjaSeleccionada : any;

	tiempoEnBanco : number;
	tiempoEspera : number;
	PersonasEnFila : number;
	probAtencionInmediata : number;

	mensajeAsignacion : string;
	 
  constructor(public navCtrl: NavController, private afs: AngularFirestore) {

  }
	
	ionViewDidLoad() {

		this.sucursales = this.afs.collection('sucursales', ref => ref.orderBy('nombre'));

		this.sucursales.valueChanges().subscribe( v =>{
		this.sucursales$ = v;
		console.log(this.sucursales$)
		});
			
		}
		
	seleccionSucursal( $event){
		console.log("-" + $event +"-")
		let aux = $event.toString().trim();
		this.franjas = this.afs.collection('franjaInfo',  ref => ref.where('sucursal', '==', aux ));
						this.franjas.valueChanges().subscribe( v =>{
							this.franjas$ = v;
							console.log(this.franjas$)
						})			
	}
	
	queue (llegada, servicio, servidores){
    var p = llegada / (servidores * servicio);
    var r = llegada / servicio;
    var p0 = 0;
    for (var i = 0; i <= servidores; i++){
        p0 += Math.pow(r, i) / this.factorial(i);
    }
    p0 += Math.pow(r, servidores) / (this.factorial(servidores)*(1-p));
    p0 = 1 / p0;
    var w = (1/servicio) + (Math.pow(r, servidores)/(this.factorial(servidores)*(servidores*servicio)*Math.pow((1-p),2)))*p0;
    var lq = ((Math.pow(r, servidores)*p)/(this.factorial(servidores)*Math.pow((1-p),2)))*p0;
    var wq = lq / llegada;
    return [w, wq, lq, p0];
  }

  factorial (n) {
    var total = 1; 
    for (var i = 1 ; i <= n ; i++) {
      total = total * i; 
    }
    return total; 
	}  

	mostrarInfo($event){  
    //franja seleccionada
    this.franjaSeleccionada = $event;

    //calculo segun teoria de colas
    var t : any[] = this.queue( $event.llegadas, $event.servicio, $event.servidores);
    //Asignacion de valores para mensajes <li>, suponiendo que esta en minutos
    this.tiempoEnBanco =  Math.floor (t[0]*60)
    this.tiempoEspera =  Math.floor (t[1]*60*60)//segundos
    this.PersonasEnFila = Math.floor(t[2])
    this.probAtencionInmediata =  Math.floor((t[3])*100 )

	}
	
	reservarTurno(){

		let aux = new Date();
    let cod = "R" +  (this.franjaSeleccionada.inicio +  this.franjaSeleccionada.fin + aux.getMilliseconds());
    //console.log(this.franjaSeleccionada.inicio + " - " + cod + " -" + this.sucursalSeleccionada.trim() + "-" );
    this.afs.collection('reservas').add(
      {
        'codigo': cod, 
        'horaLlegada': this.franjaSeleccionada.inicio,
        'sucursal' : this.sucursalSeleccionada.trim()}
			);
			
    this.mensajeAsignacion = "El código de reserva es: " + cod;
	}

}
