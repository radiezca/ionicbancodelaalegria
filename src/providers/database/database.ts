import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import * as firebase from 'firebase';
import 'firebase/firestore';
/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {

	private _DB : any;

   constructor(public http: Http)
   {
      // Initialise access to the firestore service
      this._DB = firebase.firestore();
   }

}
